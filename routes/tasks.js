import express from "express";
import { bulkMarkComplete, getTasks , addTask , getTaskById, deleteTaskById , updateTask } from "../controller/tasks.js";

const router = express.Router()

router.get('/' , getTasks);

router.get('/:id' , getTaskById);

router.post("/", addTask);

router.delete('/:id' , deleteTaskById)

router.put('/' , updateTask)

router.put('/bulkUpdate',bulkMarkComplete)

export default router;

import express from "express";
import bodyParser from "body-parser"
import taskRoutes from "./routes/tasks.js"
import mongoose from "mongoose"

const app=express()
const port=8080

app.use(bodyParser.json())
app.use('/tasks' , taskRoutes)

app.get('/' , (req,res)=>{
    res.send("Welcome to Todo App")
})

mongoose
.connect("mongodb+srv://harsh-mudgal:RJrfmXMkdlvotVKD@cluster0.hiwilik.mongodb.net/todo_nodejs?retryWrites=true&w=majority")
.then(()=>{
    console.log('connexted');
    app.listen(port,()=>console.log("808089"))
    
}).catch((e)=>{
    console.log(e)
})


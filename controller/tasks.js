import { v4 as uuidv4 } from 'uuid';

let tasks=[
    {
        "id" : "1",
        "title" : "title",
        "description": "description",
        "isCompleted": false,
        "date": "2024-01-07"
    }
];

const success={"msg":"Success"}

export const getTasks = (req,res)=>{  // send  tasks
    const {word , date}=req.query
    if(word && date){
        res.send(tasks.filter(x=> x.title.startsWith(word) && x.date===date))
    }
    else if(word){
        res.send(tasks.filter(x=> x.title.startsWith(word)))
    }
    else if(date){
        res.send(tasks.filter(x=> x.date===date))
    }
    else
        res.send(tasks)
};

export const addTask =(req,res) =>{  // add new task
    const newTask = {...req.body , id: uuidv4()}
    tasks=[...tasks , newTask]
    console.log("added")
    res.send(success)
};

export const getTaskById = (req,res)=>{  // get task gy id
    const reqParam = req.params;
    console.log(reqParam.id)
    const task= tasks.find(x => x.id===reqParam.id );
    res.send(task);
 }

export const deleteTaskById=(req,res)=>{   //delete task
    const reqParam = req.params;
    tasks=tasks.filter(x=> x.id!==reqParam.id)
    res.send(success)
};

export const updateTask = (req,res)=>{
    const newTask=req.body
    const task=tasks.find(x=>x.id===newTask.id)

    if(task==null)
        res.send("no such task")
    else{
        task.title=newTask.title
        task.description=newTask.description
        task.date=newTask.date
        task.isCompleted=newTask.isCompleted
        res.send(success)
    }
};

export const bulkMarkComplete = (req,res)=>{
    let newIds=req.query
    console.log(newIds)
    newIds=newIds.ids.split(',')
    if(newIds.length==0){
        res.send({"Enpty list of ids received":[]})
    }
    else{
        let notFound=[]
        newIds.forEach( x => {
            if(tasks.find( task => task.id===x)== null){
                notFound.push(x)
            }})

        if(notFound.length==0){
            newIds.forEach(id =>{
                const task=tasks.find(x=>x.id===id)
                task.isCompleted=!task.isCompleted
            });
            res.send(success)
        }
        else{
            res.send({'id not found': notFound})
        }
    }
};